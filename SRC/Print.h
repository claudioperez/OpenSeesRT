
#ifndef OPS_PRINT_CURRENTSTATE
#define OPS_PRINT_CURRENTSTATE 0
#define OPS_PRINT_PRINTMODEL_SECTION  1
#define OPS_PRINT_PRINTMODEL_MATERIAL 2
#define OPS_PRINT_PRINTMODEL_JSON   25000

#define OPS_PRINT_JSON_ELEM_INDENT "      "
#define OPS_PRINT_JSON_NODE_INDENT "      "
#endif
