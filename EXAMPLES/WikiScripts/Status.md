# Examples

| Status  |     File     |  Message              |
|---------|--------------|-----------------------|
|  255  | [`AxialSp`](./AxialSp) | "source AxialSp_sample.tcl" |
|  0  | [`BeamColumnJointExample`](./BeamColumnJointExample) |  |
|  0  | [`CFSSSWP`](./CFSSSWP) |  |
|  0  | [`CFSWSWP`](./CFSWSWP) |  |
|  0  | [`Chopra-10.4`](./Chopra-10.4) |  |
|  0  | [`Ex1`](./Ex1) |  |
|  0  | [`Ex3`](./Ex3) |  |
|  0  | [`Ex4`](./Ex4) |  |
|  0  | [`Ex5`](./Ex5) |  |
|  0  | [`Ex6`](./Ex6) |  |
|  0  | [`Ex7`](./Ex7) |  |
|  0  | [`Ex9`](./Ex9) |  |
|  255  | [`Excavation`](./Excavation) | "source Excavation.tcl" |
|  0  | [`FatigueMaterial`](./FatigueMaterial) |  |
|  0  | [`ForceDisplBeam`](./ForceDisplBeam) |  |
|  0  | [`InfillWall`](./InfillWall) |  |
|  255  | [`LimitStateMaterial`](./LimitStateMaterial) | invalid command name "limitCurve" |
|  0  | [`ModelingDiaphragms2D`](./ModelingDiaphragms2D) |  |
|  0  | [`MRF_Concentrated`](./MRF_Concentrated) |  |
|  0  | [`MRF_PanelZone`](./MRF_PanelZone) |  |
|  0  | [`MRF_Pushover`](./MRF_Pushover) |  |
|  0  | [`MultipleShearSpring`](./MultipleShearSpring) |  |
|  0  | [`Parallel`](./Parallel) |  |
|  0  | [`SAWSZeroLength`](./SAWSZeroLength) |  |
|  0  | [`StaticBNWFpile`](./StaticBNWFpile) |  |
|  0  | [`ViscousDamper`](./ViscousDamper) |  |
|  0  | [`1Dconsolidation.tcl`](./1Dconsolidation.tcl) |  |
|  0  | [`CB_PortalFrame.tcl`](./CB_PortalFrame.tcl) |  |
|  0  | [`DisplayModel2D.tcl`](./DisplayModel2D.tcl) |  |
|  0  | [`DisplayModel3D.tcl`](./DisplayModel3D.tcl) |  |
|  0  | [`DisplayPlane.tcl`](./DisplayPlane.tcl) |  |
|  0  | [`ElasticFrame.tcl`](./ElasticFrame.tcl) |  |
|  0  | [`Ex2a.Canti2D.ElasticElement.EQ.tcl`](./Ex2a.Canti2D.ElasticElement.EQ.tcl) |  |
|  0  | [`Ex2a.Canti2D.ElasticElement.Push.tcl`](./Ex2a.Canti2D.ElasticElement.Push.tcl) |  |
|  0  | [`Ex2b.Canti2D.InelasticSection.EQ.tcl`](./Ex2b.Canti2D.InelasticSection.EQ.tcl) |  |
|  0  | [`Ex2b.Canti2D.InelasticSection.Push.tcl`](./Ex2b.Canti2D.InelasticSection.Push.tcl) |  |
|  0  | [`Ex2c.Canti2D.InelasticFiberSection.EQ.tcl`](./Ex2c.Canti2D.InelasticFiberSection.EQ.tcl) |  |
|  0  | [`Ex2c.Canti2D.InelasticFiberSection.Push.tcl`](./Ex2c.Canti2D.InelasticFiberSection.Push.tcl) |  |
|  0  | [`Example6_4.tcl`](./Example6_4.tcl) |  |
|  255  | [`FreeFieldDamp.tcl`](./FreeFieldDamp.tcl) | expected integer but got "-dt" |
|  255  | [`FreeFieldEffective.tcl`](./FreeFieldEffective.tcl) | invalid command name "parameter" |
|  0  | [`HSSsection.tcl`](./HSSsection.tcl) |  |
|  0  | [`KikuchiAikenHDR_sample.tcl`](./KikuchiAikenHDR_sample.tcl) |  |
|  0  | [`KikuchiAikenLRB_sample.tcl`](./KikuchiAikenLRB_sample.tcl) |  |
|  255  | [`KikuchiBearing_Sample.tcl`](./KikuchiBearing_Sample.tcl) | "uniaxialMaterial AxialSp 2   1013e6 1e6 -100e6 1.00 0.01 0.50 0e6" |
|  0  | [`LateralSpreadPile.tcl`](./LateralSpreadPile.tcl) |  |
|  0  | [`MomentCurvature.tcl`](./MomentCurvature.tcl) |  |
|  0  | [`osmg.tcl`](./osmg.tcl) |  |
|  0  | [`RCFrameGravity.tcl`](./RCFrameGravity.tcl) |  |
|  0  | [`RCFramePushover.tcl`](./RCFramePushover.tcl) |  |
|  0  | [`Truss.tcl`](./Truss.tcl) |  |
|  255  | [`Wall01.tcl`](./Wall01.tcl) |    ..." |
|  0  | [`WSection.tcl`](./WSection.tcl) |  |
|  0  | [`YamamotoBiaxialHDR_Sample.tcl`](./YamamotoBiaxialHDR_Sample.tcl) |  |
